import { Pipe, PipeTransform } from '@angular/core';
import { RequestStatusEnum } from '../providers/requests/requests.types';
@Pipe({
  name: 'requestPipe'
})
export class RequestPipe implements PipeTransform {

  transform(value: RequestStatusEnum): string {
    switch(value){
      case RequestStatusEnum.Cancelada: {
        return 'Cancelada'
      }
      case RequestStatusEnum.Borrador: {
        return 'Borrador'
      }
      case RequestStatusEnum.Disponible: {
        return 'Disponible'
      }
      case RequestStatusEnum.EnCurso: {
        return 'En Curso'
      }
      case RequestStatusEnum.Finalizada: {
        return 'Finalizada'
      }
      case RequestStatusEnum.EnCursoFinal: {
        return 'En Curso Final'
      }
    }
  }

}
