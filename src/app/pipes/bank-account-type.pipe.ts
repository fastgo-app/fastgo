import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bankAccountType'
})
export class BankAccountTypePipe implements PipeTransform {

  transform(typeAccount: string): string {
    let value = parseInt(typeAccount)
    switch(value){
      case 1: {
        return 'Cuenta Corriente';
      }
      case 2: {
        return 'Cuenta Vista/Rut';
      }
      case 3: {
        return 'Chequera electronica';
      }
    }
    return null;
  }

}
