import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GraphicsPageRoutingModule } from './graphics-routing.module';

import { GraphicsPage } from './graphics.page';
import { ChartsModule } from 'ng2-charts';
import { IncomeAveragePipe } from 'src/app/pipes/income-average.pipe';
import { IncomeTotalProfitPipe } from 'src/app/pipes/income-total-profit.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GraphicsPageRoutingModule,
    ChartsModule
  ],
  declarations: [GraphicsPage, IncomeAveragePipe, IncomeTotalProfitPipe]
})
export class GraphicsPageModule {}
