import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rating'
})
export class RatingPipe implements PipeTransform {

  transform(values: number[]): number {

    if(values.length === 0)
      return (0.0)

    let rate = 0;
    for(let rating of values) {
        rate += rating;
    }
    
    return (rate / values.length)
  }
}
