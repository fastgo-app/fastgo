import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            loadChildren: () => import('../dashboard/dashboard.module').then( m => m.DashboardPageModule)
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () => import('../profile/profile.module').then( m => m.ProfilePageModule)
          }
        ]
      },
      {
        path: 'graphics',
        children: [
          {
            path: '',
            loadChildren: () => import('../graphics/graphics.module').then( m => m.GraphicsPageModule)
          }
        ]
      },
      {
        path: 'schedule',
        children: [
          {
            path: '',
            loadChildren: () => import('../schedule/schedule.module').then( m => m.SchedulePageModule)
          }
        ]
      },
      {
        path: 'create-request',
        children: [
          {
            path: '',
            loadChildren: () => import('../create-request/create-request.module').then( m => m.CreateRequestPageModule)
          }
        ]
      },
      {
        path: 'started-request',
        children: [
          {
            path: '',
            loadChildren: () => import('../started-request/started-request.module').then( m => m.StartedRequestPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/dashboard',
        pathMatch: 'full'
      }
    ]
  }, {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
