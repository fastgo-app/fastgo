import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UserType } from 'src/app/providers/accounts/accounts.types';
import { RequestsProvider } from 'src/app/providers/requests/requests.provider';
import { RequestProviderMapper } from 'src/app/providers/requests/requests.provider.mapper';
import { RequestsDTOType, RequestStatusEnum, RequestsType } from 'src/app/providers/requests/requests.types';
import { LoadingService } from 'src/app/services/loading-service/loading-service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  requestStatusEnum = RequestStatusEnum;
  user: UserType = {} as UserType;
  requests: RequestsType[];
  allRequests: RequestsType[];
  constructor(
    private requestsProvider: RequestsProvider,
    private alertController: AlertController,
    private router: Router,
    private storageService: StorageService,
    private loadingService: LoadingService,
    private requestProviderMapper: RequestProviderMapper
  ) {}

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.loadingService.presentLoading()
    this.storageService.getObj('user').then((user)=>{
      if(user.value){
        console.log(user.value)
        this.user = JSON.parse(user.value)
        this.requestsProvider.getEntries(this.user.email).then((res) => {
          if(res.status === 200) {
            //this.storageService.removeObj('lastToken')
            console.log(res.headers)
            //this.storageService.saveObj(res.headers['x-access-token'],'lastToken')
            this.requests = this.requestProviderMapper.mapArrayDTOtoArrayObj((JSON.parse(res.data) as RequestsDTOType[]))
            this.allRequests = this.requests
            console.log(this.requests)
          }
          this.loadingService.stopLoading()
        },(err)=> {
          this.loadingService.stopLoading()
          console.log(err)
        })
      }
    })
  }
  touchedRequest(request:RequestsType) {
    console.log(request.status)
    switch(request.status){
      case RequestStatusEnum.Cancelada: {
        break;
      }
      case RequestStatusEnum.Borrador: {
        this.storageService.saveObj(request,'request')
        this.router.navigate(['create-request'],{replaceUrl: true})
        break;
      }
      case RequestStatusEnum.Disponible: {
        console.log(this.user.isHaulier)
        if(this.user.isHaulier) {
          this.presentConfirmAdjudicar(request)
        }
        break;
      }
      case RequestStatusEnum.EnCurso: {
        this.storageService.saveObj(request,'request');
        this.storageService.saveObj(this.user,'user');
        this.router.navigate(['/tabs/started-request'],{replaceUrl: true})
        break;
      }
      case RequestStatusEnum.Finalizada: {
        break;
      }
      case RequestStatusEnum.EnCursoFinal: {
        this.storageService.saveObj(request,'request');
        this.storageService.saveObj(this.user,'user');
        this.router.navigate(['/tabs/started-request'],{replaceUrl: true})
        break;
      }
    }
  }
  createNewRequest(){
    this.router.navigate(['create-request'],{replaceUrl: true})
  }
  async presentConfirmAdjudicar(request: RequestsType) {
    const alert = await this.alertController.create({
      header: 'Solicitud disponible',
      message: 'Esta seguro de querer iniciar la solicitud?',
      buttons: [
          {text: 'No', role: 'cancel'},
          {text: 'Si', handler: 
            () => {
              this.storageService.getObj('lastToken').then((token)=>{
                if(token.value){
                  this.requestsProvider.updateEntry(request,this.user,JSON.parse(token.value),RequestStatusEnum.EnCurso).then((res)=>{
                    if(res.status == 200){
                      request.status = RequestStatusEnum.EnCurso
                      this.storageService.saveObj(request,'request').then(()=>{
                        this.storageService.saveObj(this.user,'user').then(()=>{
                          let token = res.headers['x-access-token']
                          this.storageService.saveObj(token,'lastToken').then(()=>{
                            this.router.navigate(['/tabs/started-request'])
                          })
                        });
                      });
                    }
                  });
                }
              })
            },
          }
      ]
    });

    await alert.present();
  }
  filterRequestByStatus(status: string){
    this.requests = []
    switch(status){
      case 'Disponible': {
        for(let request of this.allRequests){
          if(request.status === RequestStatusEnum.Disponible){
            this.requests.push(request)
          }
        }
        break;
      }
      case 'EnCurso': {
        for(let request of this.allRequests){
          if(request.status === RequestStatusEnum.EnCurso || request.status === RequestStatusEnum.EnCursoFinal){
            this.requests.push(request)
          }
        }
        break;
      }
      case 'Finalizadas': {
        for(let request of this.allRequests){
          if(request.status === RequestStatusEnum.Finalizada){
            this.requests.push(request)
          }
        }
        break;
      }
      case 'All': {
        this.requests = this.allRequests
        break;
      }
    }
    console.log(this.requests)
  }
}
