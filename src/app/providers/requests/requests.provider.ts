import { Inject, Injectable } from "@angular/core";
import { NgForm } from "@angular/forms";
import { RestService } from "src/app/services/rest/rest.service";
import { StorageService } from "src/app/services/storage/storage.service";
import { UserType } from "../accounts/accounts.types";
import { RequestProviderMapper } from "./requests.provider.mapper";
import { RequestsProviderUtils } from "./requests.provider.utils";
import { RequestStatusEnum, RequestsType } from "./requests.types";
import * as moment from 'moment'
@Injectable({
    providedIn: 'root'
})

export class RequestsProvider {
    constructor(
        private requestsProviderUtils: RequestsProviderUtils,
        @Inject(RequestProviderMapper) private requestProviderMapper: RequestProviderMapper,
        private restService: RestService,
    ){

    }
    addEntry(obj: any){
        const requestDTO = this.requestProviderMapper.mapDTOfromNgForm(obj)
        console.log(requestDTO)
        const header = {
            "x-access-token": obj.token
        }
        return this.restService.sendRequestsRequestPOSTwithToken(requestDTO,header)
    }
    updateEntry(request:RequestsType, user: UserType, token: string, status: RequestStatusEnum, income?: number){
        console.log(request)
        let body
        if(status === RequestStatusEnum.EnCursoFinal){
            body = {
                "Id": request.id
            }
        }
        else if(status === RequestStatusEnum.EnCurso){
            body = {
                "Id": request.id,
                "Email": user.email
            }
        } else  {
            body = {
                "Income": ''+income,
                "Email": user.email,
                "Id": request.id,
                "Date": moment().toISOString()
            }
        }
        const header = {
            "x-access-token": token
        }
        console.log(header)
        return this.restService.sendChangeStatusRequestGETwithToken(body,header,status);
    }
    getEntries(email: string){
        // const header = {
        //     "x-access-token": lastToken
        // }
        const body = {
            "Email": email
        }
        return this.restService.sendRequestsRequestGETwithoutToken(body)
    }
}