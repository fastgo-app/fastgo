import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { AccountProvider } from 'src/app/providers/accounts/accounts.provider';
import { AccountProviderMapper } from 'src/app/providers/accounts/accounts.provider.mapper';
import { AccountProviderUtils } from 'src/app/providers/accounts/accounts.provider.utils';
import { RegisterAccountType, UserTypeDTO } from 'src/app/providers/accounts/accounts.types';
import { AlertService } from 'src/app/services/alert/alert.service';
import { LoadingService } from 'src/app/services/loading-service/loading-service';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @ViewChild('slides') slides: IonSlides;
  user: RegisterAccountType;
  firstForm: NgForm
  constructor(
    private accountProvider: AccountProvider,
    private accountProviderUtils: AccountProviderUtils,
    private accountProviderMapper: AccountProviderMapper,
    private alertService: AlertService,
    private storageService: StorageService,
    private loadingService: LoadingService,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = {} as RegisterAccountType;
    this.user.isHaulier = true;
  }
  ionViewWillEnter(){
    this.slides.lockSwipes(true);
  }
  nextSlide(form: NgForm){
    if(!form.valid)
      return;
    
    this.firstForm = form
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  finishRegister(form: NgForm){
    if(!form.valid)
      return;

    if(!this.accountProviderUtils.checkEmailConsistency(this.user.email)){
      this.alertService.presentAlertActionCancelAndOk(
        'Correo electronico incorrecto',
        'Por favor revise su correo electronico',
        () => {return;},
        () => {return;}
      );
      return;
    }
     
    if(!this.accountProviderUtils.checkPasswordConsistency(this.user.password)){
      this.alertService.presentAlertActionCancelAndOk(
        'Contrasena',
        'La contrase;a debe ser de 5 letras minimo',
        () => {return;},
        () => {return;}
      );
      return;
    }
    

    if(!this.accountProviderUtils.checkPasswordAndConfirmPasswordEquals(this.user.password,this.user.confirmPassword)){
      this.alertService.presentAlertActionCancelAndOk(
        'Contrasena',
        'Las contrase;as son diferentes',
        () => {return;},
        () => {return;}
      );
      return;
    }
    console.log(this.firstForm.value);
    console.log(form.value);
    this.loadingService.presentLoading()
    this.accountProvider.addEntry(this.accountProviderMapper.mapRegisterFormIntoRegisterAccountTypeDTO(this.firstForm.value, form.value))
      .then((res) => {
        console.log(res)
        if(res.status === 201){
          let data = JSON.parse(res.data)
          let user = data.user as UserTypeDTO
          console.log(user)
          this.storageService.saveObj(this.accountProviderMapper.mapDTOtoObj(user), 'user');
          this.storageService.saveObj((JSON.parse(res.data)).token, 'lastToken')
          this.router.navigate(['/tabs/dashboard'],{replaceUrl:true})
        }
        this.loadingService.stopLoading()
    },(err)=> {
      this.alertService.presentAlertActionCancelAndOk('Correo electronico ya utilizado', 'El correo electronico usado ya esta en uso, por favor utiliza otro', ()=>{}, ()=>{})
      this.loadingService.stopLoading()
      console.log(err)
    })
  }
  inputRutChange(dni: string){
    if(this.user.dni != undefined || this.user.dni != null){
      let aux: string = this.user.dni.toString()
      aux = aux.replace("-","")
      if(aux.length >=1){
        let aux2 = aux.match(/([k0-9])/gi)
        let aux3 = aux2.join("")
        aux = aux3
      }
      if(aux.length >=2){
        let cuerpoRut: any;
        let dv: any;
        cuerpoRut = aux.slice(0,aux.length-1)
        dv = aux.slice(aux.length-1,aux.length)
        aux = cuerpoRut + "-" + dv
      }
      this.user.dni = aux;
    }
  }
  backSlides() {
    this.slides.getActiveIndex().then((index) => {
        if(index == 1){
          this.slides.lockSwipes(false)
          this.slides.slidePrev()
        }else{
          this.router.navigate(['login'],{replaceUrl: true})
        }
    })
  }
  emailChange(){
    this.user.email = this.user.email.replace(/\s?$/,'');
  }
  correoBancoChange(){
    this.user.correoBanco = this.user.correoBanco.replace(/\s?$/,'');
  }
}
