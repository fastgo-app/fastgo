import { Pipe, PipeTransform } from '@angular/core';
import { RequestStatusEnum, RequestsType } from '../providers/requests/requests.types';

@Pipe({
  name: 'exclusionByUserStatus'
})
export class ExclusionByUserStatusPipe implements PipeTransform {

  transform(requests: RequestsType[], isHaulier:boolean, filterStatus: RequestStatusEnum): RequestsType[] {
    if(!isHaulier)
      return requests

    return requests.filter(request => request.status !== filterStatus);
  }

}
