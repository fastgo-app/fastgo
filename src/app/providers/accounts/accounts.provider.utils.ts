import { Injectable } from "@angular/core";
import { RegisterAccountType, RegisterAccountTypeDTO } from "./accounts.types";

@Injectable({
    providedIn: 'root'
  })
export class AccountProviderUtils {
    constructor() {
        
    }
    mapRegisterAccountTypeToRegisterAccountDTO(user: RegisterAccountType) {
        let userDTO = {} as RegisterAccountTypeDTO;
        userDTO.Dni = user.dni
        userDTO.Email = user.email;
        userDTO.IsHaulier = user.isHaulier;
        userDTO.Lastnames = user.lastnames
        userDTO.Names = user.names;
        userDTO.Phone = user.phone;
        return userDTO; 
    }
    checkEmailConsistency(email: string): boolean {
        let regExp = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if(!regExp.test(email)) return false;

        return true;
    }
    checkPasswordConsistency(password: string): boolean {
        let regExp = new RegExp(/^.{5,}$/);
        if(!regExp.test(password)) return false;

        return true;
    }
    checkPasswordAndConfirmPasswordEquals(password:string, confirmPassword:string): boolean{
        return (password === confirmPassword);
    }
}