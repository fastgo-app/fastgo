import { Component, OnInit } from '@angular/core';
import { UserType } from 'src/app/providers/accounts/accounts.types';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
  user: UserType
  constructor(
    private storageService: StorageService
  ) { }

  ngOnInit() {
    this.storageService.getObj('user').then((user)=>{
      if(user.value){
        this.user = JSON.parse(user.value)
      }
    })
  }
}
