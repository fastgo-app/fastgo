import { Injectable } from "@angular/core";
import { ActionSheetController, AlertController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ActionSheetService {
    actionCancelOkClass: string;
    constructor(
        public actionSheetController: ActionSheetController
    ) {}
    async presentActionSheetTakePhotoOrSelectFromGallery(header: string, photoCallback, galleryCallback, deleteCallback,subheader?:string) {
        let actionSheet = await this.actionSheetController.create({
            header: header,
            subHeader: subheader? subheader: undefined,
            cssClass: 'my-custom-class',
            buttons: [
                {
                    text: 'Tomar foto',icon: 'camera', role:'selected',
                    handler: photoCallback
                },{
                    text: 'Galeria',icon: 'image', role:'selected',
                    handler: galleryCallback
                },{
                    text: 'Borrar',icon: 'trash', role:'destructive',
                    handler: deleteCallback
                },{
                    text: 'Cancelar',icon: 'close', role:'cancel'
                },
            ]
        })
        await actionSheet.present();
  
        const { role } = await actionSheet.onDidDismiss();
        console.log('onDidDismiss resolved with role', role);
    }
}