import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';
import { AccountProviderMapper } from 'src/app/providers/accounts/accounts.provider.mapper';
import { LoadingService } from 'src/app/services/loading-service/loading-service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule
  ],
  declarations: [LoginPage],
  providers: [AccountProviderMapper,LoadingService]
})
export class LoginPageModule {}
