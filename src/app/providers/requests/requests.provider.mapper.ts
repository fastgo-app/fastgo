import { Inject, Injectable } from "@angular/core";
import { AccountProviderMapper } from "../accounts/accounts.provider.mapper";
import { RequestsDTOType, RequestsType } from "./requests.types";
@Injectable({
    providedIn: 'root'
})
export class RequestProviderMapper {
    constructor(
        @Inject(AccountProviderMapper) private accountProviderMapper: AccountProviderMapper
    ){}
    mapArrayDTOtoArrayObj(requestsDTO: RequestsDTOType[]): RequestsType[] {
        const requests: RequestsType[] = []

        if(requestsDTO.length === 0)
            return []

        for(let requestDTO of requestsDTO){
            requests.push(this.mapDTOtoObj(requestDTO))
        }
        return requests
    }
    mapDTOtoObj(requestDTO: RequestsDTOType) : RequestsType{
        let request = {} as RequestsType
        request.cliente = requestDTO.Cliente
        request.dateFrom = requestDTO.DateFrom
        request.dateTo = requestDTO.DateTo
        request.description = requestDTO.Description
        request.dirFrom = requestDTO.DirFrom
        request.dirTo = requestDTO.DirTo
        if(requestDTO.Haulier)
            request.haulier = requestDTO.Haulier
        request.hourFrom = requestDTO.DateFrom.split('T')[1]
        request.hourTo = requestDTO.DateTo.split('T')[1]
        request.id = requestDTO._id
        request.latDirFrom = requestDTO.LatDirFrom
        request.latDirTo = requestDTO.LatDirTo
        request.lngDirFrom = requestDTO.LngDirFrom
        request.lngDirTo = requestDTO.LngDirTo
        request.load = requestDTO.Load
        request.phone = requestDTO.Phone
        request.status = requestDTO.Status
        request.value = requestDTO.Value
        return request
    }
    mapDTOfromNgForm(obj: any): RequestsDTOType{
        const request = {} as RequestsDTOType;
        console.log('id')
        console.log(obj.user.id)
        request.Cliente = obj.user.id
        request.DateFrom = obj.value.startDate.split('T')[0] + 'T' + obj.value.startHour.split('T')[1]
        request.DateTo = obj.value.endDate.split('T')[0] + 'T' + obj.value.endHour.split('T')[1]
        request.Description = obj.value.requestDescription
        request.DirFrom = obj.directions[0].dir,
        request.DirTo = obj.directions[1].dir,
        request.LatDirFrom = obj.directions[0].location.lat
        request.LngDirFrom = obj.directions[0].location.lng
        request.LatDirTo = obj.directions[1].location.lat
        request.LngDirTo = obj.directions[0].location.lng
        request.Load = obj.value.loadName
        request.Phone = obj.user.phone
        request.Status = 2
        request.Value = obj.value.requestValue
        request.Phone = obj.user.phone
        console.log(request)
        return request;
    }
}