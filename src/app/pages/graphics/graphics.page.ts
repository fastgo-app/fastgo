import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import * as moment from 'moment';
import { Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, SingleDataSet } from 'ng2-charts';
import { AccountProvider } from 'src/app/providers/accounts/accounts.provider';
import { UserType } from 'src/app/providers/accounts/accounts.types';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-graphics',
  templateUrl: './graphics.page.html',
  styleUrls: ['./graphics.page.scss'],
})
export class GraphicsPage implements OnInit {
  user: UserType = {income: []} as UserType;
  public barChartLabels: Label[] = [
    (moment().subtract(3,'month')).format('MM/YYYY'),
    (moment().subtract(2,'month')).format('MM/YYYY'),
    (moment().subtract(1,'month')).format('MM/YYYY'),
    moment().format('MM/YYYY')];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartData: ChartDataSets[] = [
    { data: [0,0,0,0], label: this.capitalize((moment().subtract(3,'month')).locale('es').format('MMMM')) },
    { data: [0,0,0,0], label: this.capitalize((moment().subtract(2,'month')).locale('es').format('MMMM')) },
    { data: [0,0,0,0], label: this.capitalize((moment().subtract(1,'month')).locale('es').format('MMMM')) },
    { data: [0,0,0,0], label: this.capitalize(moment().locale('es').format('MMMM')) }
  ];
  constructor(
    private storageService: StorageService,
    private accountProvider: AccountProvider
  ) {
    
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }
  ngOnInit() {
    
  }
  ionViewWillEnter(){
    this.storageService.getObj('user').then((user)=>{
      if(user.value){
        console.log(user.value)
        this.user = JSON.parse(user.value);
        this.processData()
      }
    })
  }
  processData(){
    for(let value of this.user.income){
      console.log(value)
      for(let i=0; i< this.barChartData.length; i++){
        console.log(this.barChartData[i])
        switch(i){
          case 0: {
            if(moment(value.date).format('YYYY') === (moment().subtract(3,'month')).format('YYYY')){
              if(moment(value.date).format('MM') === (moment().subtract(3,'month')).format('MM')){
                console.log(this.barChartData[i].data[0])
                this.barChartData[i].data[0] = parseInt(this.barChartData[i].data[0].toString()) + parseInt(value.income)
              }
                
            }
            break;
          }
          case 1: {
            if(moment(value.date).format('YYYY') === (moment().subtract(2,'month')).format('YYYY')){
              if(moment(value.date).format('MM') === (moment().subtract(2,'month')).format('MM'))
                this.barChartData[i].data[1] = parseInt(this.barChartData[i].data[1].toString()) + parseInt(value.income)
            }
            break;
          }
          case 2: {
            if(moment(value.date).format('YYYY') === (moment().subtract(1,'month')).format('YYYY')){
              if(moment(value.date).format('MM') === (moment().subtract(1,'month')).format('MM'))
                this.barChartData[i].data[2] = parseInt(this.barChartData[i].data[2].toString()) + parseInt(value.income)
            }
            break;
          }
          case 3: {
            if(moment(value.date).format('YYYY') === moment().format('YYYY')){
              if(moment(value.date).format('MM') === moment().format('MM')){
                console.log(this.barChartData[i].data[3])
                console.log(value.income)
                this.barChartData[i].data[3] = parseInt(this.barChartData[i].data[3].toString()) + parseInt(value.income)
                console.log(this.barChartData[i].data[3])
              }
                
            }
            break;
          }
        }
      }
      console.log(this.barChartData)
    }
  }
  // UTILS
  capitalize(word:string) {
    const lower = word.toLowerCase();
    return word.charAt(0).toUpperCase() + lower.slice(1);
  }
}
