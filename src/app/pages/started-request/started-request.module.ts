import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartedRequestPageRoutingModule } from './started-request-routing.module';

import { StartedRequestPage } from './started-request.page';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { RequestPipe } from 'src/app/pipes/request.pipe';
import { AccountProviderMapper } from 'src/app/providers/accounts/accounts.provider.mapper';
import { AccountProvider } from 'src/app/providers/accounts/accounts.provider';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StartedRequestPageRoutingModule,
    AgmCoreModule,
    AgmDirectionModule
  ],
  declarations: [StartedRequestPage, RequestPipe],
  providers: [AccountProvider,AccountProviderMapper]
})
export class StartedRequestPageModule {}
