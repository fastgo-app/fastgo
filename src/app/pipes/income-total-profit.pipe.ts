import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'incomeTotalProfit'
})
export class IncomeTotalProfitPipe implements PipeTransform {

  transform(incomes: any[]): unknown {
    let sum = 0
    for(let income of incomes){
      sum += parseInt(income.income)
    }
    console.log(Math.ceil(sum))
    return Math.ceil(sum);
  }

}
