import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CalendarModule } from 'ion2-calendar';
import { ChartsModule } from 'ng2-charts';
import { HTTP } from '@ionic-native/http/ngx';
import { RequestPipe } from './pipes/request.pipe';
import localeEsCL from '@angular/common/locales/es-CL'
import { registerLocaleData } from '@angular/common';
import { ExclusionByUserStatusPipe } from './pipes/exclusion-by-user-status.pipe';
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { AgmDirectionModule } from 'agm-direction';
import { BankAccountTypePipe } from './pipes/bank-account-type.pipe';
import { IncomeAveragePipe } from './pipes/income-average.pipe';
import { IncomeTotalProfitPipe } from './pipes/income-total-profit.pipe';

registerLocaleData(localeEsCL, 'es-CL')
const API_KEY = 'AIzaSyAfEoTuTPUnHIGs3_kSy1rI7lPUw7Zm9i8';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    CalendarModule.forRoot({
      doneLabel: 'Guardar',
      closeIcon: true
    }),
    ChartsModule,
    GooglePlaceModule,
    AgmCoreModule.forRoot({
      apiKey: API_KEY,
      libraries : ['places']
    }),
    AgmDirectionModule,
    
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-CL' },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, 
    HTTP,
    
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
