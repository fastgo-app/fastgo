import { Injectable } from "@angular/core";
import { RestService } from "src/app/services/rest/rest.service";
import { AccountProviderUtils } from "./accounts.provider.utils";
import { RegisterAccountType, RegisterAccountTypeDTO} from "./accounts.types";

@Injectable({
    providedIn: 'root'
})

export class AccountProvider {
    constructor(
        private accountProviderUtils: AccountProviderUtils,
        private restService: RestService
    ){

    }
    addEntry(user: RegisterAccountTypeDTO){
        return this.restService.sendUserRequestPOSTwithoutToken(user)
    }
    startSession(email: string, password: string): any {
        if(!this.accountProviderUtils.checkEmailConsistency(email)) return;

        if(!this.accountProviderUtils.checkPasswordConsistency(password)) return;
        const userLogin = {
            "Email": email,
            "Password": password
        }
        const header = {"Content-Type" : "application/json"};
        return this.restService.sendUserRequestGETwithoutToken(userLogin, header)
    }
    getInfoByUserId(userId: string){
        return this.restService.requestUserInfoGETwithoutToken({"Id": userId})
    }
}