import { UserType, UserTypeDTO } from "../accounts/accounts.types";

export interface RequestsType {
    id: string
    status: RequestStatusEnum
    cliente: string
    haulier?: string
    dateFrom: string
    dateTo: string
    hourFrom: string
    hourTo: string
    dirFrom: string
    latDirFrom: number
    lngDirFrom: number
    dirTo: string
    latDirTo: number
    lngDirTo: number
    value: number
    description: string
    load: string
    phone: string
}
export interface RequestsDTOType {
    _id: string
    Status: RequestStatusEnum
    Cliente: string
    Haulier?: string
    DateFrom: string
    DateTo: string
    DirFrom: string
    LatDirFrom: number
    LngDirFrom: number
    DirTo: string
    LatDirTo: number
    LngDirTo: number
    Value: number
    Description: string
    Load: string
    Phone: string
}
export enum RequestStatusEnum {
    Cancelada = 0,
    Borrador = 1,
    Disponible = 2,
    EnCurso = 3,
    Finalizada = 4,
    EnCursoFinal = 5,
}