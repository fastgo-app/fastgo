import { Injectable } from "@angular/core";
import { LoadingController } from "@ionic/angular";

@Injectable({
    providedIn: 'root'
})
export class LoadingService {
    constructor(
        private loadingController: LoadingController
    ){

    }
    async presentLoading() {
        const loading = await this.loadingController.create({
          cssClass: 'my-custom-class',
          duration: 5000
        });
        await loading.present();
    }
    async stopLoading(){
        this.loadingController.dismiss()
    }
}