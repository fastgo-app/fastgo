import { Component, OnInit, ViewChild } from '@angular/core';
import { BackgroundGeolocationPlugin } from "@capacitor-community/background-geolocation";
import { UserType, UserTypeDTO } from 'src/app/providers/accounts/accounts.types';
import { RequestsType } from 'src/app/providers/requests/requests.types';
import { StorageService } from 'src/app/services/storage/storage.service';
import { RequestStatusEnum } from 'src/app/providers/requests/requests.types';
import { registerPlugin } from '@capacitor/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { RequestsProvider } from 'src/app/providers/requests/requests.provider';
import { Router } from '@angular/router';
import { AccountProvider } from 'src/app/providers/accounts/accounts.provider';
import { AccountProviderMapper } from 'src/app/providers/accounts/accounts.provider.mapper';
import * as moment from 'moment';
import { AgmMap } from '@agm/core';
const BackgroundGeolocation = registerPlugin<BackgroundGeolocationPlugin>("BackgroundGeolocation");
@Component({
  selector: 'app-started-request',
  templateUrl: './started-request.page.html',
  styleUrls: ['./started-request.page.scss'],
})
export class StartedRequestPage implements OnInit {
  @ViewChild('map') map: AgmMap
  income: number;
  requestStatusEnum = RequestStatusEnum
  request: RequestsType;
  user: UserType
  haulier: UserType
  client: UserType
  location = {
    lng: 0,
    lat: 0, 
  }
  watcher
  mapLocation = {
    lng: -0,
    lat: 0, 
  }
  mapStartDir: any[] = []
  loadedPosition: boolean;
  userPosition = {
    lng: 0,
    lat: 0, 
  }
  constructor(
    private accountProvider: AccountProvider,
    private accountProviderMapper: AccountProviderMapper,
    private storageService: StorageService,    
    private alertController: AlertController,
    private requestProvider: RequestsProvider,
    private toastController: ToastController,
    private loadingController: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.storageService.getObj('request').then((request)=>{
      if(request.value){
        let newRequest: RequestsType = JSON.parse(request.value)
        console.log(newRequest);
        this.request = newRequest
        this.storageService.removeObj('request')
        this.mapStartDir[0] = {
          lng:this.request.lngDirFrom,
          lat:this.request.latDirFrom
        }
        this.mapStartDir[1] = {
          lng:this.request.lngDirTo,
          lat:this.request.latDirTo
        }
        console.log(this.mapStartDir)
        
        this.storageService.getObj('user').then((user)=>{
          if(user.value){
            let newUser: UserType = JSON.parse(user.value)
            this.user = newUser
            console.log(this.user)
            let idToSend
            if(this.user.isHaulier){
              idToSend = this.request.cliente
            }else{
              idToSend = this.request.haulier
            }
            console.log(idToSend)
            this.presentLoading()
            this.accountProvider.getInfoByUserId(idToSend).then((res)=> {
              this.loadingController.dismiss()
              if(res.status === 200){
                if(this.user.isHaulier){
                  this.client = this.accountProviderMapper.mapDTOtoObj(JSON.parse(res.data) as UserTypeDTO)
                }else{
                  this.haulier = this.accountProviderMapper.mapDTOtoObj(JSON.parse(res.data) as UserTypeDTO)
                }
              }
            }, (err) => {
              this.loadingController.dismiss()
              console.log(err);
            })
          }
        })
      }
      console.log(this.mapStartDir)
    })
    this.watcher = BackgroundGeolocation.addWatcher(
      {
        backgroundMessage: "Cancel to prevent battery drain.",
        backgroundTitle: "Tracking You.",
        requestPermissions: true,
        stale: false,
        distanceFilter: 0
      },
      (location, error) => {
        if (error) {
            if (error.code === "NOT_AUTHORIZED") {
                if (window.confirm(
                    "This app needs your location, " +
                    "but does not have permission.\n\n" +
                    "Open settings now?"
                )) {
                    BackgroundGeolocation.openSettings();
                }
            }
            return console.error(error);
        }
        this.location = {
          lng: location.longitude,
          lat: location.latitude
        }
        if(!this.loadedPosition){
          this.userPosition = this.location
          this.loadedPosition = true
        }
        this.mapStartDir[0] = this.location
        this.mapStartDir[1] = {
          lng:this.request.lngDirFrom,
          lat:this.request.latDirFrom
        }
        return console.log(location);
      }
    )
  }
  ionViewWillLeave() {
    this.watcher.then((watcher_id) => {
      BackgroundGeolocation.removeWatcher({
          id: watcher_id
      });
    });
  }
  async recogerCarga() {
    const alert = await this.alertController.create({
        header: 'Confirmar Carga',
        message: 'Esta seguro de que posee la carga?',
        buttons: [
          {
              text: 'Cancelar', role: 'cancel', 
              handler: () => {}
          },
        
          {
            text: 'Ok',
            handler: () => {
              this.presentLoading()
              this.storageService.getObj('lastToken').then((token) => {
                if(token.value){
                  this.requestProvider.updateEntry(this.request, this.user, JSON.parse(token.value), RequestStatusEnum.EnCursoFinal).then((res) => {
                    this.loadingController.dismiss()
                    if(res.status == 200) {
                      this.storageService.removeObj('lastToken')
                      let token = res.headers['x-access-token']
                      this.storageService.saveObj(token,'lastToken')
                      this.request.status = RequestStatusEnum.EnCursoFinal
                      this.mapStartDir[1] = {
                        lng:this.request.lngDirTo,
                        lat:this.request.latDirTo
                      }                  
                    }
                  })
                }
              })
            }
          }
        ]
    });
  
    await alert.present();
  }
  async endRequest() {
    console.log(this.income)
    if(!this.income || this.income === 0){
      this.presentToast('Por favor ingresa un monto recibido valido.')
      return;
    }
    const alert = await this.alertController.create({
        header: 'Confirmar Entrega',
        message: 'Esta seguro de que entregaste la carga?',
        buttons: [
          {
              text: 'Cancelar', role: 'cancel', 
              handler: () => {}
          },
        
          {
            text: 'Ok',
            handler: () => {
              this.presentLoading()
              this.storageService.getObj('lastToken').then((token) => {
                if(token.value){
                  console.log(JSON.parse(token.value))
                  this.requestProvider.updateEntry(this.request, this.user, JSON.parse(token.value), RequestStatusEnum.Finalizada, this.income).then((res) => {
                    this.loadingController.dismiss()
                    if(res.status == 200) {
                      this.user.income.push({
                        date: moment().toISOString(),
                        income: this.income
                      })
                      this.storageService.removeObj('user')
                      this.storageService.saveObj(this.user, 'user')
                      this.storageService.removeObj('lastToken')
                      let token = res.headers['x-access-token']
                      console.log(token)
                      this.storageService.saveObj(token,'lastToken')
                      this.presentToast('Felicitaciones por terminar la solicitud!')
                      this.router.navigate(['/tabs/dashboard'], {replaceUrl: true});
                    }
                  })
                }
              })
            }
          }
        ]
    });
  
    await alert.present();
  }
   async presentLoading() {
    const toast = await this.loadingController.create({
      duration: 2000
    });
    await toast.present();
  }
  async presentToast(text:string) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }
  startCall(phone){
    window.open('tel:+569'+phone);
  }
}
