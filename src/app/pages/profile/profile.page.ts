import { Component, OnInit } from '@angular/core';
import { ActionSheetService } from 'src/app/services/action-sheet/action-sheet.service';
import { Camera, CameraResultType } from '@capacitor/camera';
import { UserType } from 'src/app/providers/accounts/accounts.types';
import { StorageService } from 'src/app/services/storage/storage.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user:UserType = {} as UserType
  constructor(
    private actionSheetService: ActionSheetService,
    private storageService: StorageService,
    private router: Router
  ) {
    this.user.names = '-'
    this.user.lastnames = '-'
    this.user.dni = '-'
    this.user.email = '-'
    this.user.phone = '-'
    this.user.rating = [0,0,0,0]
   }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.storageService.getObj('user').then((user)=>{
      if(user.value){
        this.user = JSON.parse(user.value)
        console.log('b')
        console.log(this.user)
      }
    });
  }
  async newPhoto() {
    await this.actionSheetService.presentActionSheetTakePhotoOrSelectFromGallery(
      'Subir Avatar',
      async () => {
        const image = await Camera.getPhoto({
          quality: 50,
          allowEditing: true,
          resultType: CameraResultType.Uri
        });
        let imageUrl = image.webPath;
        this.user.avatar = imageUrl;
        //this.accountProvider.photoChanged()
      },
      async () => {
        const image = await Camera.pickImages({
          quality: 50,
          limit: 1
        });
        let imageUrl = image.photos[0].webPath;
        this.user.avatar = imageUrl;
        //this.accountProvider.photoChanged()
      },
      async () => {
        this.user.avatar = '';
        //this.accountProvider.deleteAvatarFromUserId()
      }
    );
  }
  closeAccount(){
    this.storageService.removeObj('user').then(()=>{
      this.storageService.removeObj('lastToken').then(()=>{
        this.router.navigate(['/login'], {replaceUrl:true});
      })
    })
  }
}
