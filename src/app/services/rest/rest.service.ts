import { Injectable } from "@angular/core";
import { HTTP } from '@ionic-native/http/ngx';
import { RequestStatusEnum } from "src/app/providers/requests/requests.types";

const DEFAULT_URL = 'https://api-fastgo-skovs.ondigitalocean.app/'
const DEFAULT_USERS_ENDPOINT = 'api/v1/users'
const DEFAULT_REQUESTS_ENDPOINT = 'api/v1/requests'
const DEFAULT_API_ENDPOINT = 'api/v1/'

@Injectable({
    providedIn: 'root'
})
export class RestService {
    constructor(
        private http : HTTP,
    ) {

    }
    requestUserInfoGETwithoutToken(objToBody:any, header?:any){
        this.http.setDataSerializer('json')
        return this.http.get(
            DEFAULT_URL + DEFAULT_USERS_ENDPOINT + '/get-info', objToBody, header
        );
    }
    sendUserRequestGETwithoutToken(objToBody:any, header?:any){
        this.http.setDataSerializer('json')
        console.log(objToBody)
        return this.http.get(
            DEFAULT_URL + DEFAULT_USERS_ENDPOINT, objToBody, header
        );
    }
    sendUserRequestPOSTwithoutToken(objToBody:any, header?:any){
        return this.http.post(
            DEFAULT_URL + DEFAULT_USERS_ENDPOINT, objToBody, header
        );
    }
    sendRequestsRequestGETwithoutToken(objToBody:any, header?:any) {
        console.log(objToBody)
        return this.http.get(
            DEFAULT_URL + DEFAULT_REQUESTS_ENDPOINT, objToBody, header
        );
    }
    sendChangeStatusRequestGETwithToken(objToBody:any, header?:any, status?: RequestStatusEnum) {
        console.log(objToBody)
        switch(status){
            case RequestStatusEnum.EnCurso: {
                return this.http.get(
                    DEFAULT_URL + DEFAULT_REQUESTS_ENDPOINT + '/start_request', objToBody, header
                );
            }
            case RequestStatusEnum.EnCursoFinal: {
                return this.http.get(
                    DEFAULT_URL + DEFAULT_REQUESTS_ENDPOINT + '/request_final_dir', objToBody, header
                );
            }
            case RequestStatusEnum.Finalizada: {
                return this.http.get(
                    DEFAULT_URL + DEFAULT_REQUESTS_ENDPOINT + '/end_request', objToBody, header
                );
            }
        }
        
    }
    sendRequestsRequestPOSTwithToken(objToBody:any, header?:any) {
        return this.http.post(
            DEFAULT_URL + DEFAULT_REQUESTS_ENDPOINT, objToBody, header
        );
    }
}