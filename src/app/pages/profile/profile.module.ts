import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilePageRoutingModule } from './profile-routing.module';

import { ProfilePage } from './profile.page';
import { RatingPipe } from 'src/app/pipes/rating.pipe';
import { BankAccountTypePipe } from 'src/app/pipes/bank-account-type.pipe';
import { IncomeTotalProfitPipe } from 'src/app/pipes/income-total-profit.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilePageRoutingModule
  ],
  declarations: [ProfilePage, RatingPipe, BankAccountTypePipe, IncomeTotalProfitPipe]
})
export class ProfilePageModule {}
