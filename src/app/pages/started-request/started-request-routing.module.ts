import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartedRequestPage } from './started-request.page';

const routes: Routes = [
  {
    path: '',
    component: StartedRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartedRequestPageRoutingModule {}
