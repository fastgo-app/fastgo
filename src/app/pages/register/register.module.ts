import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterPageRoutingModule } from './register-routing.module';

import { RegisterPage } from './register.page';
import { LoadingService } from 'src/app/services/loading-service/loading-service';
import { AccountProviderMapper } from 'src/app/providers/accounts/accounts.provider.mapper';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterPageRoutingModule
  ],
  declarations: [RegisterPage],
  providers: [AccountProviderMapper,LoadingService]
})
export class RegisterPageModule {}
