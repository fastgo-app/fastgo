import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { StorageService } from './services/storage/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private storageService: StorageService,
    private router: Router
  ) {this.initializeApp()}

  initializeApp(){
    this.platform.ready().then(()=> {
      this.storageService.getObj('user').then((user) => {
        if(user.value){
          console.log(user.value)
          this.router.navigate(['/tabs/dashboard'], {replaceUrl: true})
        }
      })
    })
  }
}
