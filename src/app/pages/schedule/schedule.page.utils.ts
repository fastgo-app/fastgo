import { Injectable } from "@angular/core";
import { RequestsType } from "src/app/providers/requests/requests.types";

@Injectable({
    providedIn: 'root'
})
export class SchedulePageUtils {
    constructor(){}
    searchRequestsByDay(date:string, allUserRequests:RequestsType[]): RequestsType[] {
        const requestsSelected: RequestsType[] = []
        if(allUserRequests.length <= 0)
            return []

        for(const request of allUserRequests) {
            if(request.dateFrom.split('T')[0] === date){
                requestsSelected.push(request)
            }
        }
        return requestsSelected;
    }
}