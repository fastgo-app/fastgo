import { Component, Input, OnInit } from '@angular/core';
import { CalendarComponent, CalendarComponentOptions } from 'ion2-calendar';
import * as moment from 'moment'
import { UserType } from 'src/app/providers/accounts/accounts.types';
import { RequestsProvider } from 'src/app/providers/requests/requests.provider';
import { RequestProviderMapper } from 'src/app/providers/requests/requests.provider.mapper';
import { RequestsDTOType, RequestStatusEnum, RequestsType } from 'src/app/providers/requests/requests.types';
import { LoadingService } from 'src/app/services/loading-service/loading-service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { SchedulePageUtils } from './schedule.page.utils';
@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {
  @Input('calendar') calendar: CalendarComponent
  user: UserType
  requests: RequestsType[]
  requestsByDay: RequestsType[] = []
  requestStatusEnum = RequestStatusEnum
  daySelected: string
  calendarOptions: CalendarComponentOptions  = {
    daysConfig: [],
    color: 'primary',
    weekStart: 1,
    showMonthPicker: false,
    weekdays: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
  };
  schedulerDone:boolean = false
  constructor(
    private scheduleUtils: SchedulePageUtils,
    private loadingService: LoadingService,
    private storageService: StorageService,
    private requestsProvider: RequestsProvider,
    private requestsProviderMapper: RequestProviderMapper
  ) { }

  ngOnInit() {
  }
  ionViewDidEnter(){
    this.loadingService.presentLoading()
    this.storageService.getObj('user').then((user)=>{
      if(user.value){
        console.log(user.value)
        this.user = JSON.parse(user.value)
        this.requestsProvider.getEntries(this.user.email).then((res) => {
          if(res.status === 200) {
            //this.storageService.removeObj('lastToken')
            console.log(res.headers)
            //this.storageService.saveObj(res.headers['x-access-token'],'lastToken')
            this.requests = this.requestsProviderMapper.mapArrayDTOtoArrayObj((JSON.parse(res.data) as RequestsDTOType[]))
            console.log(this.requests)
            for(let request of this.requests){
              let date = new Date(request.dateFrom)
              
              this.calendarOptions.daysConfig.push({
                date: date,
                marked: true,
                subTitle: this.capitalize(request.load)
              })
            }
            
            this.schedulerDone = true
            console.log(this.calendarOptions.daysConfig)
          }
          this.loadingService.stopLoading()
        },(err)=> {
          this.loadingService.stopLoading()
          console.log(err)
        })
      }
    })
  }
  onChange(date) {
    this.daySelected = date.toJSON().split('T')[0];
    console.log("dia diaSelected: ", this.daySelected);
    this.requestsByDay = this.scheduleUtils.searchRequestsByDay(this.daySelected, this.requests)
  }
  // UTILS
  capitalize(word:string) {
    const lower = word.toLowerCase();
    return word.charAt(0).toUpperCase() + lower.slice(1);
  }
}
