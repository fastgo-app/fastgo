export interface RegisterAccountType {
    names: string,
    lastnames: string,
    phone: string,
    email: string,
    password: string,
    confirmPassword: string,
    dni: string
    isHaulier: boolean,
    nombreBanco: string;
    cuentaBanco: string;
    correoBanco: string;
    tipoCuenta: string;
}
export interface RegisterAccountTypeDTO {
    Names: string,
    Lastnames: string,
    Phone: string,
    Email: string,
    Password: string,
    Dni: string
    IsHaulier: boolean,
    NombreBanco: string;
    CuentaBanco: string;
    CorreoBanco: string;
    TipoCuenta: string;
    Income: any[]
}
export interface UserType {
    id: string
    avatar: string,
    names: string,
    lastnames: string,
    phone: string,
    email: string,
    dni: string,
    isHaulier: boolean,
    rating: number[]
    nombreBanco: string;
    cuentaBanco: string;
    correoBanco: string;
    tipoCuenta: string;
    income: any[];
}
export interface UserTypeDTO {
    _id: string
    Avatar: string,
    Names: string,
    Lastnames: string,
    Phone: string,
    Email: string,
    Dni: string,
    IsHaulier: boolean,
    Rating: number[]
    NombreBanco: string;
    CuentaBanco: string;
    CorreoBanco: string;
    TipoCuenta: string;
    Income: any[];
}