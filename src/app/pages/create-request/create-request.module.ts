import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateRequestPageRoutingModule } from './create-request-routing.module';

import { CreateRequestPage } from './create-request.page';
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { AgmDirectionModule } from 'agm-direction';
import { RequestProviderMapper } from 'src/app/providers/requests/requests.provider.mapper';

const API_KEY = 'AIzaSyAfEoTuTPUnHIGs3_kSy1rI7lPUw7Zm9i8';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateRequestPageRoutingModule,
    GooglePlaceModule,
    AgmCoreModule,
    AgmDirectionModule
  ],
  declarations: [CreateRequestPage],
  providers: [RequestProviderMapper]
})
export class CreateRequestPageModule {}
