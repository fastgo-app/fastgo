import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { RequestPipe } from 'src/app/pipes/request.pipe';
import { ExclusionByUserStatusPipe } from 'src/app/pipes/exclusion-by-user-status.pipe';
import { RequestProviderMapper } from 'src/app/providers/requests/requests.provider.mapper';
import { RequestsProvider } from 'src/app/providers/requests/requests.provider';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule
  ],
  declarations: [DashboardPage, RequestPipe, ExclusionByUserStatusPipe],
  providers:[RequestsProvider, RequestProviderMapper]
})
export class DashboardPageModule {}
