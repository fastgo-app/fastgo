import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { AccountProvider } from 'src/app/providers/accounts/accounts.provider';
import { AccountProviderMapper } from 'src/app/providers/accounts/accounts.provider.mapper';
import { UserTypeDTO } from 'src/app/providers/accounts/accounts.types';
import { RequestsDTOType } from 'src/app/providers/requests/requests.types';
import { AlertService } from 'src/app/services/alert/alert.service';
import { LoadingService } from 'src/app/services/loading-service/loading-service';
import { StorageService } from 'src/app/services/storage/storage.service';
import { AccountProviderUtils } from '../../providers/accounts/accounts.provider.utils'
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('slide') slide: IonSlides;
  email: string = '';
  password: string = '';
  constructor(
    private accountProvider: AccountProvider,
    private accountProviderUtils: AccountProviderUtils,
    private accountProviderMapper: AccountProviderMapper,
    private storageService: StorageService,
    private alertService: AlertService,
    private router: Router,
    private loadingService: LoadingService
  ) { }

  ngOnInit() {
  }
  logIn() {
    if(this.email === ''){
      this.alertService.presentAlertActionCancelAndOk('Error!', 'Ingrese su correo electronico', ()=>{}, ()=>{})
      return
    }

    if(this.email === ''){
      this.alertService.presentAlertActionCancelAndOk('Error!', 'Ingrese su contraseña', ()=>{}, ()=>{})
      return
    }

    if(!this.accountProviderUtils.checkEmailConsistency(this.email)){
      this.alertService.presentAlertActionCancelAndOk('Email erroneo', 'Ingrese correctamente su correo', ()=>{}, ()=>{})
      return;
    }
    
    if(!this.accountProviderUtils.checkPasswordConsistency(this.password)){
      this.alertService.presentAlertActionCancelAndOk('Contraseña erronea', 'La contrase;a debe tener 5 caracteres minimos', ()=>{}, ()=>{})
      return;
    }

    this.loadingService.presentLoading()
    this.accountProvider.startSession(this.email, this.password).then((res) => {
      this.loadingService.stopLoading()
      if(res.status == 200){
        console.log(res)
        let result = JSON.parse(res.data)
        let accountDTO = result.result as UserTypeDTO
        console.log(accountDTO)
        this.storageService.saveObj(this.accountProviderMapper.mapDTOtoObj(accountDTO), 'user').then(()=>{
          this.storageService.saveObj(result.token, 'lastToken').then(() => {
            this.router.navigate(['/tabs/dashboard'],{replaceUrl: true})
          })
        })
      }
    }, (err) => {
      this.alertService.presentAlertActionCancelAndOk('Error!', 'Existe un error con tu correo electronico o tu contraseña, por favor reintenta', ()=>{}, ()=>{})
      this.loadingService.stopLoading()
      console.log(err)
    })
  }
  registerPage(){
    this.router.navigate(['/register'],{replaceUrl: true})
  }
  emailChange(email){
    console.log(email.detail.value)
    console.log(this.email)
    this.email = this.email.replace(/\s?$/,'');
  }
}
