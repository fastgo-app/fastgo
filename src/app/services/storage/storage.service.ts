import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }
  async saveObj(obj:any, key:string){
    await Storage.set({
      key: key,
      value: JSON.stringify(obj),
    });
  }
  async getObj(key:string){
    const value  = await Storage.get({ key: key });
    return value
  }
  async removeObj(key:string){
    await Storage.remove({ key: key });
  }
}
