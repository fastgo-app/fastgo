import { Injectable } from "@angular/core";
import { AlertController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AlertService {
    actionCancelOkClass: string;
    constructor(
        private alertController: AlertController
    ) {
        this.actionCancelOkClass = ''
    }
    async presentAlertActionCancelAndOk(header: string, body: string, cancelCallback, okCallback, textPositiveHandler?:string, textNegativeHandler?: string) {
        const alert = await this.alertController.create({
          cssClass: this.actionCancelOkClass,
          header: header,
          message: body,
          buttons: [
              {text: textNegativeHandler ? textNegativeHandler:'Cancelar', role: 'cancel', handler: cancelCallback},
              {text: textPositiveHandler ? textPositiveHandler:'Ok', handler: okCallback}
          ]
        });
    
        await alert.present();
      }
}