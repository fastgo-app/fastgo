import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'incomeAverage'
})
export class IncomeAveragePipe implements PipeTransform {

  transform(incomes: any[]): unknown {
    let sum = 0
    for(let income of incomes){
      sum += parseInt(income.income)
    }
    if(sum === 0)return 0
    
    return Math.ceil(sum/incomes.length);
  }

}
