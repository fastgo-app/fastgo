import { Injectable } from "@angular/core";
import { RegisterAccountTypeDTO, UserType, UserTypeDTO } from "./accounts.types";
@Injectable({
    providedIn: 'root'
})
export class AccountProviderMapper {
    constructor(){}

    mapDTOtoObj(userDTO: UserTypeDTO): UserType{
        let user = {} as UserType
        if(userDTO.Avatar)
            user.avatar = userDTO.Avatar
        if(userDTO.CorreoBanco)
            user.correoBanco = userDTO.CorreoBanco
        if(userDTO.TipoCuenta){}
            user.tipoCuenta = userDTO.TipoCuenta
        if(userDTO.CuentaBanco)
            user.cuentaBanco = userDTO.CuentaBanco
        if(userDTO.NombreBanco)
            user.nombreBanco = userDTO.NombreBanco
        user.dni = userDTO.Dni
        user.email = userDTO.Email
        user.id = userDTO._id
        user.isHaulier = userDTO.IsHaulier
        user.lastnames = userDTO.Lastnames
        user.names = userDTO.Names
        user.rating = userDTO.Rating
        user.phone = userDTO.Phone
        user.income = userDTO.Income
        return user
    }
    mapRegisterFormIntoRegisterAccountTypeDTO(firstForm:any, secondForm: any): RegisterAccountTypeDTO{
        let userDTO = {} as RegisterAccountTypeDTO;
        userDTO.Names = firstForm.Name
        userDTO.Lastnames = firstForm.Lastname
        userDTO.Phone = firstForm.Phone
        userDTO.Password = secondForm.Password
        userDTO.Dni = secondForm.Dni
        userDTO.Email = secondForm.Email
        userDTO.IsHaulier = secondForm.Haulier
        userDTO.CorreoBanco = secondForm.CorreoBanco
        userDTO.CuentaBanco = secondForm.CuentaBanco
        userDTO.TipoCuenta = secondForm.TipoCuenta
        userDTO.NombreBanco = secondForm.NombreBanco
        return userDTO;
    }
}