import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { UserType } from 'src/app/providers/accounts/accounts.types';
import { RequestsProvider } from 'src/app/providers/requests/requests.provider';
import { RequestProviderMapper } from 'src/app/providers/requests/requests.provider.mapper';
import { RequestStatusEnum, RequestsType } from 'src/app/providers/requests/requests.types';
import { AlertService } from 'src/app/services/alert/alert.service';
import { StorageService } from 'src/app/services/storage/storage.service';
@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.page.html',
  styleUrls: ['./create-request.page.scss'],
})
export class CreateRequestPage implements OnInit {
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  user: UserType;
  request: RequestsType = {
  } as RequestsType
  direcctionInput: Address;
  directions = []
  options = {
    types : [],
    componentRestrictions: { country: 'CL' }
  }
  startDate: string;
  endDate: string;
  lat = -33.025697
  lng = -71.547616
  constructor(
    public toastController: ToastController,
    public alertService: AlertService,
    private requestProvider: RequestsProvider,    
    private alertController: AlertController,
    private router: Router,
    private storageService: StorageService,
    // private requestProviderMapper: RequestProviderMapper
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.storageService.getObj('user').then((user)=>{
      if(user.value){
        this.user = JSON.parse(user.value)
      }
    });
    this.storageService.getObj('request').then((request)=>{
      if(request.value){
        console.log('1')
        let newRequest: RequestsType = JSON.parse(request.value)
        this.request = newRequest
        this.storageService.removeObj('request')
      }
    })
  }
  public handleAddressChange(address: Address) {
    let location = {lat: address.geometry.location.lat(), lng: address.geometry.location.lng()}
    if(this.directions.length == 2){
      this.presentToast('Elimine una direccion antes de agregar otra por favor.')
      return
    }

    this.directions.push(
      {
        dir: address.formatted_address,
        location
      }
    )

    this.lat = address.geometry.location.lat();
    this.lng = address.geometry.location.lng();
    
    this.direcctionInput = undefined
  }
  confirmDelete(dir: any){
    this.presentConfirmDelete(dir)
  }
  async presentToast(text:string) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }
  async presentConfirmDelete(dir) {
    const alert = await this.alertController.create({
      header: 'Eliminar direccion',
      message: 'Esta seguro de querer eliminar la direccion?',
      buttons: [
          {text: 'No', role: 'cancel'},
          {text: 'Si', handler: 
          () => {
            for(let i=0;i<=this.directions.length; i++){
              if(dir.dir === this.directions[i].dir){
                this.directions.splice(i, 1)
                return;
              }
            }
            this.presentToast('Direccion eliminada correctamente.')
          },}
      ]
    });

    await alert.present();
  }
  async presentConfirmSaveRequest() {
    const alert = await this.alertController.create({
      header: 'Solicitud borrador',
      message: 'Desea guardar esta solicitud como borrador?',
      buttons: [
          {text: 'No', role: 'cancel' , handler: () => {
            this.router.navigate(['/tabs/dashboard'], {replaceUrl:true})// add user state
          }},
          {text: 'Si', handler: 
          () => {
            //this.requestProvider.addEntry();
          },}
      ]
    });

    await alert.present();
  }
  askSaveOrNotBorrador(){
    this.router.navigate(['/tabs/dashboard'], {replaceUrl:true})
    //this.presentConfirmSaveRequest()
  }
  createNewRequest(form: NgForm) {
    console.log(form);
    console.log(this.directions)
    if(!form.valid){
      return;
    }

    if(this.directions.length < 2){
      this.presentToast('Porfavor ingresa como minimo dos direcciones.')
      return;
    }
    this.storageService.getObj('lastToken').then((token)=>{
      if(token.value){
        this.requestProvider.addEntry(
          {
            value:form.value,
            directions:this.directions,
            user:this.user,
            token: JSON.parse(token.value)
          }).then((res) => {
            if(res.status === 200){
              console.log(res.headers)
              console.log(res.data)
              this.storageService.removeObj('lastToken')
              let token = res.headers['x-access-token']
              this.storageService.saveObj(token,'lastToken')
              this.presentToast('Solicitud creada exitosamente.')
              this.router.navigate(['/tabs/dashboard'],{replaceUrl: true});
            }
          })
      }
    });
  }
  dateFromChange(date) {
    console.log(date)
    if(date.detail > this.request.dateTo)
      this.request.dateTo = ''
  }
  hourFromChange(date) {
    if(date.detail > this.request.hourTo)
      this.request.hourTo = ''
  }
  // valueChange(value){
  //   this.request.value = parseInt(this.request.value.toString().replace(/[^\d,.]+$/, ''))
  // }
}
